package it.lundstedt.erik.myFirst112Mod.blocks;

import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.myFirst112Mod.MyFirst112Mod;
import it.lundstedt.erik.myFirst112Mod.items.MyItems;
import it.lundstedt.erik.myFirst112Mod.tabs.MyTabs;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemBlock;

import java.util.ArrayList;
import java.util.List;


public class MyBlocks
{
	public static List<Block> blocks=new ArrayList<>();
	public static final Block console = registerBlock("console",new Block(Material.ANVIL));
	public static Block registerBlock(String name, Block block)
	{
		block.setUnlocalizedName(name);
		block.setRegistryName(MyFirst112Mod.MODID,name);
		blocks.add(block);
		return block;
	}
	public static Block registerBlock(String name, Block block, CreativeTabs tab)
	{
		Block newBlock=registerBlock(name, block);
		MyItems.registerItem(name,new ItemBlock(newBlock),tab);
		return newBlock;
	}
	
	
	
	public static void init () {
		Menu.drawItem("loading blocks...", MyFirst112Mod.printer);
		Menu.drawItem("...done loading blocks", MyFirst112Mod.printer);
	}



}
