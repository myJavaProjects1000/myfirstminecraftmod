package it.lundstedt.erik.myFirst112Mod.customMenu;

import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.myFirst112Mod.utils.Utilities;
import net.minecraft.entity.player.EntityPlayer;
public class MyMenu extends Menu
{
	public static void setup(EntityPlayer playerIn){
		drawingObj.printer = Utilities.getChatConsumer(playerIn);
	}
	public static void drawItems(String[]menuItems)
	{
		for (String menuItem : menuItems)
		{
			drawingObj.content(menuItem);
		}
	}
	public static void drawItem(String menuItem)
	{
		drawingObj.content(menuItem);
	}
	public static void drawHeader(String[]header)
	{
		drawingObj.headder(header);
	}
}
