package it.lundstedt.erik.myFirst112Mod.utils;

import java.io.IOException;
import java.util.Random;

import it.lundstedt.erik.myFirst112Mod.MyFirst112Mod;
import okhttp3.*;
	
	
	public class Net {
		public static void get(NetSettings settings) throws IOException {
			String key=settings.getKey();
			String method=settings.getMethod();
			String url=settings.getUrl();
			OkHttpClient client = new OkHttpClient().newBuilder()
					.build();
			MediaType mediaType = MediaType.parse("text/plain");
			RequestBody body = RequestBody.create(mediaType, "");
			Request request = new Request.Builder()
					.url(url)
					.method(method, body)
					.addHeader("Authorization", "Bearer "+key)
					.build();
			Response response = client.newCall(request).execute();
			
			MyFirst112Mod.logger.info(response.body().string());
		}


}
