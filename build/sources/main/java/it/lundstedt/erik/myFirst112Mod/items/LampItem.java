package it.lundstedt.erik.myFirst112Mod.items;

import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.myFirst112Mod.MyFirst112Mod;
import it.lundstedt.erik.myFirst112Mod.utils.Net;
import it.lundstedt.erik.myFirst112Mod.utils.ToggleSettings;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

import java.io.IOException;
import java.util.Random;

public class LampItem extends Item {
	public LampItem()
	{
		this.setCreativeTab(CreativeTabs.MISC);
		this.setUnlocalizedName("Lamp");
		this.setMaxStackSize(1);
	}
	/**
	 * Called when the equipped item is right clicked.
	 */
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		ItemStack shellStack=playerIn.getHeldItem(handIn);
		if (!worldIn.isRemote)
		{
			Menu.drawItem("oooh fancy", MyFirst112Mod.printer);
			try {
				Net.get(new ToggleSettings());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (worldIn.isRemote)
		{

		}
		return ActionResult.newResult(EnumActionResult.SUCCESS,shellStack);
	}
}
