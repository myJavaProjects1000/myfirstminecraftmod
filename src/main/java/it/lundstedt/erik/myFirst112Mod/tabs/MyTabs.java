package it.lundstedt.erik.myFirst112Mod.tabs;

import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.myFirst112Mod.MyFirst112Mod;
import it.lundstedt.erik.myFirst112Mod.items.MyItems;
import it.lundstedt.erik.myFirst112Mod.utils.Utilities;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class MyTabs
{
	public static CreativeTabs tab=new CreativeTabs(MyFirst112Mod.NAME) {
		@SideOnly(Side.CLIENT)
		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(Utilities.getItemSuplier(MyItems.shell).get());
		}
	};
	public static void init ()
	{
		Menu.drawItem("loading tabs...", MyFirst112Mod.printer);
		Menu.drawItem("...done loading tabs", MyFirst112Mod.printer);
	}






}
