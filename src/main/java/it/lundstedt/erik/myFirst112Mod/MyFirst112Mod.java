package it.lundstedt.erik.myFirst112Mod;
import it.lundstedt.erik.menu.*;
import it.lundstedt.erik.myFirst112Mod.tabs.MyTabs;
import it.lundstedt.erik.myFirst112Mod.utils.Net;
import it.lundstedt.erik.myFirst112Mod.utils.ToggleSettings;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStoppingEvent;
import org.apache.logging.log4j.Logger;

import java.util.function.Consumer;

@Mod(modid = MyFirst112Mod.MODID, name = MyFirst112Mod.NAME, version = MyFirst112Mod.VERSION)
public class MyFirst112Mod {
	public static final String MODID = "myfirst112mod";
	public static final String NAME = "MyFirst112Mod";
	public static final String VERSION = "0.0.0.1";
/*
	public static Thread getterThread = new Thread(new Runnable()
	{
		@Override public void run() {
	}
	public void jwpost(){
		try {
			Net.get(new ToggleSettings());
		} catch (Exception e) {e.printStackTrace();}
		getterThread.interrupt();
		}
	
	});
 */
	
	
	public static Logger logger;
	public static Consumer<String> printer = text ->{logger.info(text);};
	@EventHandler
	public void preInit(FMLPreInitializationEvent event){
		logger = event.getModLog();
		MyTabs.init();
	}

	@EventHandler
	public void init(FMLInitializationEvent event){
		// some example code
		//logger.info("DIRT BLOCK >> {}", Blocks.DIRT.getRegistryName());
		//logger.info("hello World");
		Menu logg=new Menu();
		
		logg.setPrinter(printer);
		logg.licence(MODID,VERSION,printer);
		Menu.licence(printer);
		
	}
	
	

	
	
}
