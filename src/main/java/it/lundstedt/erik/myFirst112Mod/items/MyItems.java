package it.lundstedt.erik.myFirst112Mod.items;

import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.myFirst112Mod.MyFirst112Mod;
import it.lundstedt.erik.myFirst112Mod.tabs.MyTabs;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

import java.util.ArrayList;
import java.util.List;

public class MyItems {
	public static List<Item> items=new ArrayList<>();
	
	public static final Item shell=registerItem("remoteCloudShell",new RemoteCloudShellItem(), MyTabs.tab);
	public static final Item lamp=registerItem("Lamp",new LampItem(),MyTabs.tab);
	public static Item registerItem(String name, Item item, CreativeTabs tab)
	{
		item.setUnlocalizedName(name);
		item.setRegistryName(MyFirst112Mod.MODID,name);
		item.setCreativeTab(tab);
		items.add(item);
		return item;
	}
	
	
	public static void init() {
		Menu.drawItem("loading items...", MyFirst112Mod.printer);
		Menu.drawItem("...done loading items", MyFirst112Mod.printer);
	}
	
	
	
	
}
