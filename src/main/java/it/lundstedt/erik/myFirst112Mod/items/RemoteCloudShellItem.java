package it.lundstedt.erik.myFirst112Mod.items;

import it.lundstedt.erik.menu.Menu;
import it.lundstedt.erik.myFirst112Mod.MyFirst112Mod;
import it.lundstedt.erik.myFirst112Mod.customMenu.MyMenu;
import it.lundstedt.erik.myFirst112Mod.utils.Utilities;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.World;

import java.util.function.Consumer;

public class RemoteCloudShellItem extends Item {
	public RemoteCloudShellItem()
	{
		this.setCreativeTab(CreativeTabs.MISC);
		this.setUnlocalizedName("newBourneShell");
		//Bourne again shell
		this.setMaxStackSize(1);
		
	}
	/**
	 * Called when the equipped item is right clicked.
	 */
	@Override
	public ActionResult<ItemStack> onItemRightClick(World worldIn, EntityPlayer playerIn, EnumHand handIn) {
		ItemStack shellStack=playerIn.getHeldItem(handIn);
		if (!worldIn.isRemote)
		{
			MyMenu.drawItem("hello there", Utilities.getChatConsumer(playerIn));
			
		}
		return ActionResult.newResult(EnumActionResult.SUCCESS,shellStack);
	}
}
